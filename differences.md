# My approximation differs from the one provided by the BIKE authors
The authors of BIKE provide their own approximations of the threshold function for BIKE 1/2 levels 1, 3 and 5. See the [spec](https://bikesuite.org/files/round2/spec/BIKE-Spec-2019.06.30.1.pdf) for more details. My approximations differ from theirs.

## Comparison of the approximations
Here are the graphs of the approximations. Green lines correspond to the approximations provided by the authors, red lines are my approximations, blue points are the actual values of the threshold function.

**Level 1**

![BIKE 1/2 LEVEL 1](images/bike_12_lvl_1.png)

| Approximation by | slope | intercept |
|------------------|-------|-----------| 
| authors | 0.0069722 | 13.53 |
| me | 0.00688204906 | 9.07994442 |

**Level 3**

![BIKE 1/2 LEVEL 3](images/bike_12_lvl_3.png)

| Approximation by | slope | intercept |
|------------------|-------|-----------| 
| authors | 0.0052936 | 15.932 |
| me | 0.00471665999 | 10.8419897 |

**Level 5**

![BIKE 1/2 LEVEL 5](images/bike_12_lvl_5.png)

| Approximation by | slope | intercept |
|------------------|-------|-----------| 
| authors | 0.0043536 | 17.489 |
| me | 0.00373043420 | 13.1891959 |

As we can see, my approximations seem to be more accurate. It would obviously be much better to use higher degree approximation, but that was not the point of this work.

## Where I could have messed up?
- **bugs** The approximations are obviously correct based on the displayed data. It is however entirely possible, the "real" data of the threshold function are not correctly calculated. I implemented both calculations using explicit formula and inequality (details in the spec). These always gave me the same results, which means that the only possible mistake could have happened in the calculation of the $`X, \pi_0,`$ and $`\pi_1`$ values. However, I found no bugs in those.
- **my understanding of the threshold** I may simply not understand the threshold and/or BG decoders
- **difference in slopes** in levels 3 and 5 can be explained if the authors used different range for syndrome weight. All that the spec says on the matter is "in the relevant range for the syndrome weight", neglecting to specify that range. My ranges go up to 0.7*r, which is arguably too high.

I have no idea what else could have happened on my side. I would highly appreciate any input on the matter.

## Does it matter?
No. It just bugs me. No pun intended.

Longer answer: in my testing (albeit not too extensive as of yet) the BG decoders seem to prefer higher thresholds than what's predicted by the threshold function specified in the spec. My approximations give lower values for the threshold function, which makes them inferior to the ones provided by the authors regardless of their accuracy (if they are indeed correct).
