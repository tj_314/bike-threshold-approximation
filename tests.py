import main
import unittest
import math


class TestThreshold(unittest.TestCase):
    def test_threshold_constructor(self):
        t = main.Threshold(r=400, w=26, t=22)
        self.assertEqual(t._r, 400)
        self.assertEqual(t._n, 800)
        self.assertEqual(t._w, 26)
        self.assertEqual(t._t, 22)
        self.assertEqual(t._d, 13)
        self.assertTrue(math.isclose(t._X, 22.114694794554758, rel_tol=1e-7, abs_tol=0.0))

        t = main.Threshold(r=150, w=51, t=72)
        self.assertEqual(t._r, 150)
        self.assertEqual(t._n, 300)
        self.assertEqual(t._w, 51)
        self.assertEqual(t._t, 72)
        self.assertEqual(t._d, 25)
        self.assertTrue(math.isclose(t._X, 843, rel_tol=1e-7, abs_tol=0.0))

    def test_threshold_start_stop_syndrome_weight(self):
        t = main.Threshold(r=11779, w=142, t=134)
        self.assertEqual(t.start_syndrome_weight, 7)
        self.assertEqual(t.get_start_syndrome_weight(), 7)
        self.assertEqual(t.stop_syndrome_weight, 8245)
        self.assertEqual(t.get_stop_syndrome_weight(), 8245)

    def test_threshold_calculate_pi0(self):
        t = main.Threshold(r=150, w=51, t=72)  # X=843
        pi0 = t.calculate_pi0(50)
        self.assertTrue(math.isclose(pi0, 0.2994736842, rel_tol=1e-7, abs_tol=0.0))
        pi0 = t.calculate_pi0(20)
        self.assertTrue(math.isclose(pi0, 0.03105263158, rel_tol=1e-7, abs_tol=0.0))

    def test_threshold_calculate_pi1(self):
        t = main.Threshold(r=150, w=51, t=72)  # X=843
        pi1 = t.calculate_pi1(49)
        self.assertTrue(math.isclose(pi1, 0.4955555556, rel_tol=1e-7, abs_tol=0.0))
        pi1 = t.calculate_pi1(24)
        self.assertTrue(math.isclose(pi1, 0.4816666667, rel_tol=1e-7, abs_tol=0.0))

    def test_threshold_calculate_inequality(self):
        t = main.Threshold(r=15, w=13, t=5)
        self.assertEqual(t.calculate_threshold_inequality(3), 4)
        self.assertEqual(t.calculate_threshold_inequality(2), 3)

    def test_threshold_calculate_formula(self):
        t = main.Threshold(r=15, w=13, t=5)
        self.assertEqual(t.calculate_threshold_formula(3), 4)
        self.assertEqual(t.calculate_threshold_formula(2), 3)

    def test_threshold_calculate_equal_values(self):
        differ = False
        t = main.Threshold(r=40597, w=274, t=264)
        for weight in range(t.get_start_syndrome_weight(), t.get_stop_syndrome_weight()):
            formula_threshold = t.calculate_threshold_formula(weight)
            inequality_threshold = t.calculate_threshold_inequality(weight)
            if formula_threshold != inequality_threshold and verbose:
                differ = True
        self.assertFalse(differ)


if __name__ == "__main__":
    unittest.main()
