# Threshold approximation for BG decoders
## Introduction
As a part of my bachelor's thesis, I implemented a black-gray (BG) decoder as specified in this [paper](https://eprint.iacr.org/2019/1423.pdf). Part of the decoding process is calculating a value called threshold. Decoding is an iterative process and this value is recalculated in every iteration.

The threshold is the smallest integer T which satisfies the following inequality:

$`t{d \choose T}\pi_1^T(1-\pi_1)^{d-T} \ge (n-t){d \choose T}\pi_0^T(1-\pi_0)^{d-T}`$

It can be explicitly calculated as follows:

$`
T = \left\lceil \frac{log\frac{n-t}{t} + d*log\frac{1-\pi_0}{1-\pi_1}}{log\frac{\pi_1}{\pi_0} + log\frac{1-\pi_0}{1-\pi_1}} \right\rceil
`$,

where $`\pi_1 = \frac{|s|+X}{td}`$, $`\pi_0=\frac{w|s|-X}{(n-t)d}`$ and $`X = \sum_{l\ odd}(l-1)\frac{r{w \choose l}{n-w \choose t-l}}{n \choose t}`$. Furthermore, we have constants $`r, w, t, d=w/2`$ and $`n=2r`$. Value $`|s|`$ changes every iteration. For futher deatils, refer to the [BIKE spec](https://bikesuite.org/files/round2/spec/BIKE-Spec-2019.06.30.1.pdf).

The threshold function can be approximated as a linear function of $`|s|`$. The [BIKE spec](https://bikesuite.org/files/round2/spec/BIKE-Spec-2019.06.30.1.pdf) provides 3 approximations relevant for my work. However, I needed more approximations for various combinations of the parameters $`r, w, t`$. This project calculates these approximations.

## How to run
Download the sources and run them using python. You need to have Python 3.9.x, matplotlib and numpy installed.

## Status
This is a simple script, not yet providing accurrate approximations. It is under active development. Constructive feedback is highly appreciated.

**MY APPROXIMATIONS differ from the ones provided by the BIKE authors!!** Here's my [writeup](differences.md).