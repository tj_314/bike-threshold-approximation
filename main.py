__author__ = "Tomáš Vavro"
__license__ = "BSD-3-Clause"

import matplotlib.pyplot as plt
from scipy.special import comb
import numpy as np


class Threshold:
    def __init__(self, r, w, t):
        self._r = r
        self._w = w
        self._t = t
        self._n = 2 * self._r
        self._d = self._w // 2

        # calculate X
        self._X = 0
        bound = min(w, t)  # l must be leq than w and t
        if bound % 2 == 1:
            bound += 1
        for l_val in range(3, bound, 2):
            tmp = (l_val - 1) * comb(self._w, l_val, exact=True) * comb(self._n - self._w, self._t - l_val, exact=True)
            self._X += tmp
        self._X *= self._r
        self._X /= comb(self._n, self._t, exact=True)
        self.start_syndrome_weight = int(self._X / self._w) + 1  # smallest syndrome weight such that pi0 is positive
        self.stop_syndrome_weight = int(0.7 * self._r)

    def get_start_syndrome_weight(self):
        return self.start_syndrome_weight

    def get_stop_syndrome_weight(self):
        return self.stop_syndrome_weight

    def calculate_pi0(self, syn_weight):
        return (self._w * syn_weight - self._X) / ((self._n - self._t) * self._d)

    def calculate_pi1(self, syn_weight):
        return (syn_weight + self._X) / (self._t*self._d)

    def calculate_threshold_formula(self, syn_weight):
        pi0 = self.calculate_pi0(syn_weight)
        pi1 = self.calculate_pi1(syn_weight)
        tmp = (1 - pi0) / (1 - pi1)
        top_left_log = np.log((self._n - self._t) / self._t)
        bottom_left_log = np.log(pi1 / pi0)
        t_val = (top_left_log + self._d * np.log(tmp)) / (bottom_left_log + np.log(tmp))
        return np.ceil(t_val)

    def calculate_threshold_inequality(self, syn_weight):
        pi0 = self.calculate_pi0(syn_weight)
        pi1 = self.calculate_pi1(syn_weight)
        for th in range(self._d + 1):
            tmp = comb(self._d, th, exact=True)
            left_side = self._t * tmp * (pi1 ** th) * ((1 - pi1) ** (self._d - th))
            right_side = (self._n - self._t) * tmp * (pi0 ** th) * ((1 - pi0) ** (self._d - th))
            if left_side >= right_side:
                return th
        return None


def approximate(threshold, draw_graph=True, author_predicted_a=None, author_predicted_b=None, start_syndrome_weight=None, stop_syndrome_weight=None):
    if stop_syndrome_weight is None:
        stop_syndrome_weight = threshold.get_stop_syndrome_weight()
    if start_syndrome_weight is None:
        start_syndrome_weight = threshold.get_start_syndrome_weight()
    x = range(start_syndrome_weight, stop_syndrome_weight)

    # calculate actual values of the threshold function
    y = []
    for i in x:
        th = threshold.calculate_threshold_formula(i)
        y.append(th)

    if draw_graph:
        plt.scatter(x, y)
        plt.ylabel("threshold")
        plt.xlabel("syndrome weight")

    # machine learning
    model = np.polyfit(x, y, 1)  # linear regression
    print(model)  # result coefficients

    if draw_graph:
        predict = np.poly1d(model)
        predict_y = predict(x)
        plt.plot(x, predict_y, c="r")

        # actual approximation by the BIKE authors for comparison reasons
        if (author_predicted_a is not None) and (author_predicted_b is not None):
            author_approx = np.poly1d(np.array([author_predicted_a, author_predicted_b]))
            author_y = author_approx(x)
            plt.plot(x, author_y, c="g")

        plt.show()


if __name__ == "__main__":
    # BIKE 1/2, LEVEL 1
    approximate(Threshold(r=11779, w=142, t=134), author_predicted_a=0.0069722, author_predicted_b=13.530)

    # BIKE 1/2, LEVEL 3
    # approximate(Threshold(r=24821, w=206, t=199), author_predicted_a=0.0052936, author_predicted_b=15.932)

    # BIKE 1/2, LEVEL 5
    # approximate(Threshold(r=40597, w=274, t=264), author_predicted_a=0.0043536, author_predicted_b=17.489)
